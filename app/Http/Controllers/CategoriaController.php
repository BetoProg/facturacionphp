<?php


namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;

use sisVentas\Http\Requests;

use sisVentas\Http\Requests\CategoriaFormRequest;

use sisVentas\Categoria;

use DB;

header("Access-Control-Allow-Origin: *");

class CategoriaController extends Controller
{
    public function __construct(){
        $this->middleware('cors');
    }

    public function index(){
        $categoria = Categoria::all();
        return response()->json(['data'=>$categoria],200);
    }

    public function show($palabra){
        $categoria = Categoria::where('nombre','LIKE',"%{$palabra}%")
        ->orderby('idcategoria','=','desc')
        ->where('condicion','1')->get();

        if(!$categoria){
            return response()->json(['error'=>array(['code'=>400,'message',
            'No se encuentra la categoria'])],400);
        }

          return response()->json(['status'=>'ok','data'=> $categoria],200);
    }

    public function store(Request $request){
        $categoria = new Categoria;

        $categoria->nombre=$request->input('nombre');
        $categoria->descripcion=$request->input('descripcion');
        $categoria->condicion=$request->input('condicion');

        try {

            $categoria->save();
            return response()->json(['status'=>'ok','data'=> 'se ha ingresado tu registro'],200);

        } catch (\Exception $ex) {

            return response()->json(['error'=>array(["debus"=>$request->get('descripcion'),'code'=>400,'message',$ex])],400);
        }

    }

    public function edit($id){
        $categoria = Categoria::findOrFail($id);

        if(!$categoria){
            return response()->json(['error'=>array(['code'=>400,'message',
            'No se encuentra la categoria'])],400);
        }

        return response()->json(['status'=>'ok','data'=>$categoria],200);
    }

    public function update(CategoriaFormRequest $request){
        $categoria = Categoria::findOrFail($request->get('idcategoria'));
        /*if(!$categoria){
            return response()->json(['message'=>'no se encontro registro con ese id'],200);
        }*/

        $categoria->nombre=($request->get('nombre')!='' ? $request->get('nombre') : $categoria->nombre);
        $categoria->descripcion=($request->get('descripcion') !='' ? $request->get('descripcion'):$categoria->descripcion);
        $categoria->condicion='1';

        try {

            $categoria->update();
            return response()->json(['status'=>'ok','data'=> 'se ha actualizado tu registro'],200);

        } catch (\Exception $ex) {

            return response()->json(['error'=>array(['code'=>400,'message',$ex])],400);

        }
    }

    public function destroy($id){
        $categoria = Categoria::findOrFail($id);
        if(!$categoria){
          return response()->json(['message'=>'no se encontro registro con ese id'],200);
        }

        $categoria->condicion='0';

        try {

            $categoria->update();
            return response()->json(['status'=>'ok','data'=> 'se ha eliminado tu registro'],200);

        } catch (\Exception $ex) {

            return response()->json(['error'=>array(['code'=>400,'message',$ex])],400);

        }
    }
}
