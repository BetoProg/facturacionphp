<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;
use sisVentas\Http\Requests;

use sisVentas\Http\Requests\CategoriaFormRequest;

use sisVentas\Categoria;

use Illuminate\Support\Facades\DB;


class CategoriaViewController extends Controller
{
    public function index(Request $request){

        if($request){
            $palabra= trim($request->get('palabra'));

            $categorias = DB::table('categoria')->where('nombre','LIKE','%'.$palabra.'%')
                ->where('condicion','=','1')->orderby('idcategoria','asc')->get();

            return view('almacen.categoria.index',["categorias"=>$categorias,"searchText"=>$palabra]);
        }

    }

    public function create(){
        return view('almacen.categoria.create');
    }

    public function store(Request $request){
        $categoria=new Categoria;
        $categoria->nombre=$request->get('nombre');
        $categoria->descripcion=$request->get('descripcion');
        $categoria->condicion='1';
        $categoria->save();

        return Redirect::to('almacen/categoria');
    }

    public function show($id){
        return view("almacen.categoria.show",["categoria"=>Categoria::findOrFail($id)]);
    }

    public function edit($id){
        return view("almacen.categoria.editar",["categoria"=>Categoria::findOrFail($id)]);
    }

    public function update(CategoriaFormRequest $request,$id){
        $categoria=Categoria::findOrFail($id);
        $categoria->nombre=$request->get('nombre');
        $categoria->descripcion=$request->get('descripcion');
        $categoria->update();

        return Redirect::to('almacen/categoria');
    }

    public function destroy($id){
        $categoria=Categoria::findOrFail($id);

        $categoria->condicion='0';
        $categoria->update();
        return Redirect::to('almacen/categoria');
    }

}
