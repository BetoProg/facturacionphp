<?php
header("Access-Control-Allow-Origin: *");
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('almacen/categoria','CategoriaViewController');

/*Route::get('api/categoria','CategoriaController@index');
Route::get('api/categoria/{palabra?}','CategoriaController@show');
Route::get('api/edit/categoria/{id}','CategoriaController@edit');
Route::post('api/categoria','CategoriaController@store');
Route::put('api/categoria','CategoriaController@update');
Route::delete('api/categoria','CategoriaController@destroy');*/
