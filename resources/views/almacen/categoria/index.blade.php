@extends('layaout.master')
@section('content')
    <h2>Lista de Categorias</h2>

    <a href="categoria/create" class="btn btn-success btn-sm">
        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
    </a>
    <br><br>
    <div class="row">
        <div class="col-lg-8 col-sm-8 col-xs-12">
            @include('almacen.categoria.search')
        </div>
    </div>
    <table class="table table-striped table-condensed">
        <thead>
        <tr>
            <th>id</th>
            <th>Nombre</th>
            <th>Descripcion</th>
            <th>Opciones</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categorias as $cat)
            <tr>
                <td>{{$cat->idcategoria}}</td>
                <td>{{$cat->nombre}}</td>
                <td>{{$cat->descripcion}}</td>
                <td>
                    <a href="{{URL::action('CategoriaViewController@edit',$cat->idcategoria)}}"><button class="btn btn-info">Editar</button></a>
                    <a href="" data-target="#modal-delete-{{$cat->idcategoria}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a
                </td>
            </tr>
        @include('almacen.categoria.modal')
        @endforeach
        </tbody>
    </table>
@stop